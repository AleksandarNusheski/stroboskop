# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://AleksandarNusheski@bitbucket.org/AleksandarNusheski/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/ec80c55a27a473f53615cdf315410e3feb0b3d5e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/0f48a4045e9e70fb718210a312307959d95d6032

Naloga 6.3.2:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/2e8f648b67ab4fb874021134adc0cdfeb66f7f3f

Naloga 6.3.3:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/fa477d5012b0d9fbb89c54ad6a2185203ce7d002

Naloga 6.3.4:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/a508eb4d0080639f0cc40ecce8659bd7da2fdf2e

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/30c5b882b1fc36349b5ae525e297351f11b5bece

Naloga 6.4.2:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/cc15e8415bab21ebf962b9a8bad26d2432392cfb

Naloga 6.4.3:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/0e1a354bdb2f4065859736599c92b184f4d01c4d

Naloga 6.4.4:
https://bitbucket.org/AleksandarNusheski/stroboskop/commits/d2008a4fbea9d561aa28d170c08c1328ee71ad68